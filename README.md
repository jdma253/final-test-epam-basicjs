# Final Test EPAM JS COURSE

Foobar is a Python library for dealing with word pluralization.

## Installation

Use the console to create your project

```bash
npm init
```

```bash
npm install typescript --save-dev
```

```bash
npm install rxjs

```

```bash
npm install -g json-server

```

```json
{
  "name": "task",
  "version": "1.0.0",
  "description": "\"last task EPAM\"",
  "main": "index.js",
  "scripts": {
    "start": "json-server --watch db.json",
    "test": "test"
  },
  "keywords": [
    "\"juanda\""
  ],
  "author": "Juan David Moreno Alfonso",
  "license": "ISC",
  "devDependencies": {
    "typescript": "^4.9.5"
  },
  "dependencies": {
    "rxjs": "^7.8.0"
  }
}

```
```bash
> task@1.0.0 start
> json-server --watch db.json


  \{^_^}/ hi!

  Loading db.json
  Done

  Resources
  http://localhost:3000/tasks

  Home
  http://localhost:3000
```